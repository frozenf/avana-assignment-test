<?php

/**
 * Function to check where the index of closing brackets given by opening position
 *
 * @param String $str
 * @param int $index
 * @return int
 */
function checkClosingBracketsPosition(String  $str, int $index) : int
{
    # Check if given index is opening brackets or not
    if($str[$index] != '(')
        return -1;

    # Variable to count open brackets
    $openBracket = 1;

    # this loop begin from given index + 1
    for ($i=$index+1;$i<strlen($str);$i++)
    {
        # If current character is closing bracket then reduce the openBracket value
        if ($str[$i] == ')')
            $openBracket--;

        # If current character is opening bracket then increase the openBracket value
        if ($str[$i] == '(')
            $openBracket++;

        # If there are no longer open bracket then return the current index as the index of closing brackets.
        if ($openBracket == 0)
            return $i;
    }

    # if non of closing bracket are found from given character and index then return -1
    return -1;
}

echo checkClosingBracketsPosition('a (b c (d e (f) g) h) i (j k)',20);