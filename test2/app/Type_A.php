<?php
/**
 * Created By PHPSTORM
 * Name : Farih Nazihullah
 * Date : 02/04/2022 15:29
 *
 */

namespace App;

/**
 * Class Type_A
 * Package for Checking Type A Excel
 * @package App
 */
class Type_A extends Base_Type
{
    protected string $files = "files/Type_A.xlsx";
}