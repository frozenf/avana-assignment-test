<?php

namespace App;
use ExcelChecker\ExcelChecker;

class Base_Type
{
    protected String $files = "";

    public function validateExcel()
    {
        $validateResult = new ExcelChecker($this->files);
        $errors = $validateResult->getValidateResult();

        if (count($errors) > 0)
        {
            echo "<table style='margin-bottom: 10px;' border='1'><thead><tr><td>Row</td><td>Error</td></tr></thead>";
            echo "<tbody>";
            foreach($errors as $key => $res)
            {
                echo "<tr>";
                echo "<td>$key</td>";
                echo "<td>".implode(',',$res)."</td>";
                echo "</tr>";
            }
            echo "<tbody";
            echo "</table>";
        }


    }
}