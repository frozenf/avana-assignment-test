<?php

require __DIR__ . '/../vendor/autoload.php';

use App\Type_A;
use App\Type_B;

$type_a = new Type_A();
$type_a->validateExcel();

$type_b = new Type_B();
$type_b->validateExcel();