<?php
/**
 * Created By PHPSTORM
 * Name : Farih Nazihullah
 * Date : 02/04/2022 15:24
 *
 */

namespace ExcelChecker;

use PhpOffice\PhpSpreadsheet\Exception;

class ExcelChecker
{
    protected String $path;

    protected array $headers = [];

    protected array $rules = [];

    protected array $errors = [];

    public function __construct (String $path)
    {
        $this->path = $path;
    }

    public function getValidateResult () : array
    {
        $data = $this->importExcelToArray();

        # Convert Header and Set Rule
        $this->checkRuleHeader();

        # Validate Data
        $this->validation($data);

        # Return Errors
        return  $this->errors;
    }

    protected function importExcelToArray() : array
    {
        # Init XLSX Reader
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        # Set Read Only
        $reader->setReadDataOnly(false);

        $reader->setReadEmptyCells(false);

        # Read Spreadsheet
        $spreadsheet = $reader->load(__DIR__ . '/../../' . $this->path);

        try {
            # Get Data from First Index
            $sheet = $spreadsheet->getSheet($spreadsheet->getFirstSheetIndex());

            # Convert Data to Excel
            $data = $sheet->toArray(null,true,true,false);

            # Header Data
            $this->headers = array_shift($data);

            return $data;

        } catch (Exception $e) {
            return [];
        }
    }

    protected function checkRuleHeader()
    {
        foreach ($this->headers as $key => $header)
        {
            $this->rules[$key] = [];

            if (str_contains($header,'*'))
                $this->rules[$key][] = 'required';

            if (str_contains($header,'#'))
                $this->rules[$key][] = 'no_space';

            $this->headers[$key] = preg_replace("/[^a-zA-Z0-9_]+/", "", $header);
        }
    }

    protected function validation(array $data)
    {
        foreach ($data as $rowIndex => $value)
        {
            foreach ($value as $colIndex => $string)
            {
                $this->validateRequired($rowIndex,$colIndex,$string);

                $this->validateNoSpace($rowIndex,$colIndex,$string);
            }
        }
    }

    protected function validateRequired(int $rowIndex, int $colIndex, $str)
    {
        if (in_array('required', $this->rules[$colIndex])) {
            if (empty($str))
                $this->errors[$rowIndex + 1][] = "Missing value in ".$this->headers[$colIndex];
        }
    }

    protected function validateNoSpace(int $rowIndex, int $colIndex, $str = '')
    {
        $str = $str ?? '';
        if (in_array('no_space', $this->rules[$colIndex])) {
            if (str_contains($str,' '))
                $this->errors[$rowIndex + 1][] = $this->headers[$colIndex]." should not contain any space";
        }
    }

}